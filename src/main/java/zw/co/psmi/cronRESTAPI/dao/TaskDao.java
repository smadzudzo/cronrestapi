/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.psmi.cronRESTAPI.dao;

import zw.co.psmi.cronRESTAPI.entity.Task;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author b1tfr34k
 */
@Repository
//public interface TaskDao extends CrudRepository<Task, Long> {
public interface TaskDao extends JpaRepository<Task, Long> {
//    List<Task> findByTaskCmd(String cmd);

    @Transactional
    List<Task> deleteBytId(long tId);

    @Transactional
    List<Task> findByActive(Boolean active);

    @Transactional
    @Query(value = "SELECT cmd FROM tasks WHERE taskTime IS NOT NULL", nativeQuery = true)
    List<Task> findCmdByTime();

    @Transactional
    @Query(value = "SELECT taskTime FROM tasks WHERE taskTime IS NOT NULL", nativeQuery = true)
    List<Task> findTimeByTime();
    
//    @Transactional
//    @Query(value = "?1", nativeQuery = true)
//    List<Task> findQuery(String query);
    
}
