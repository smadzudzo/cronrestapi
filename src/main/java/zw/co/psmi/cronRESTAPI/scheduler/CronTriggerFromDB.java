package zw.co.psmi.cronRESTAPI.scheduler;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.annotations.common.util.impl.Log_$logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import zw.co.psmi.cronRESTAPI.entity.Task;
import zw.co.psmi.cronRESTAPI.dao.TaskDao;
import zw.co.psmi.cronRESTAPI.service.TaskService;

/**
 * This class is used for executing multiple quartz job using CronTrigger(Quartz
 * 2.1.5).
 *
 * @author javawithease
 */
@Service
public class CronTriggerFromDB {

    private static final Logger logger = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    TaskDao repo;

    public void CronTriggerSchedule() {
        String cmd = "";
        String cronValue = "";
        long tId = 0L;
        int count = 0;

        List<Task> task = repo.findByActive(Boolean.TRUE);

        for (Task t : task) {
            tId = t.tId;
            cmd = t.cmd;
            cronValue = t.taskTime;
            try {
                JobDetail job1 = JobBuilder.newJob(CronJob.class).withIdentity(String.valueOf(tId), "group" + tId).build();
                Trigger trigger1 = TriggerBuilder.newTrigger()
                        .withIdentity("cronTrigger" + tId, "group" + tId)
                        .withSchedule(CronScheduleBuilder
                        .cronSchedule(cronValue)).build();
                Scheduler scheduler = new StdSchedulerFactory().getScheduler();
                JobDataMap jobDataMap = job1.getJobDataMap();
                jobDataMap.put(CronJob.COMMAND, cmd);
                if (!scheduler.checkExists(job1.getKey())) {
                    scheduler.start();
                    scheduler.scheduleJob(job1, trigger1);
                } else {
                    scheduler.deleteJob(job1.getKey());
                    scheduler.start();
                    scheduler.scheduleJob(job1, trigger1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            count = count + 1;
        }
    }
}
