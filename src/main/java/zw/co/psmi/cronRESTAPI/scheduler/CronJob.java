/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.psmi.cronRESTAPI.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import zw.co.psmi.cronRESTAPI.dao.TaskDao;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import zw.co.psmi.cronRESTAPI.service.TaskService;

/**
 *
 * @author b1tfr34k
 */
public class CronJob implements Job {
    
    private static final Logger logger = LoggerFactory.getLogger(CronJob.class);
    public static final String COMMAND = "cmd";

//    @Autowired
//    private TaskDao taskDao;
//    @Autowired
//    TaskService taskService;
//    @Autowired
//    JdbcTemplate jdbcTemplate;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        String cmd = dataMap.getString(COMMAND);

        logger.info("Job triggered by task in db with command(cmd): '{}'", cmd);
//        logger.info("Job triggered");
    }
}
