/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.psmi.cronRESTAPI.service;

import java.util.List;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import zw.co.psmi.cronRESTAPI.entity.Task;

/**
 *
 * @author b1tfr34k
 */
@Service
public interface TaskService {

    public Task createTask(@RequestBody Task task);

    public Task editTask(@RequestBody Task task);

    public Task getById(@RequestParam("tId") long tId);

    public List<Task> getAll();

    public List<Task> remById(@RequestParam("tId") long tId);

    public List<Task> delByReallyDeletingFromTheDataBaseService(@RequestParam("tId") long tId);

    public Iterable<Task> getAllEvenInactive();

//    public List<Task> getTaskByQuery(JobExecutionContext context) throws JobExecutionException;
}
