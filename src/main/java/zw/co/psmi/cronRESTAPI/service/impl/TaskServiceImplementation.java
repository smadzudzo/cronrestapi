/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.psmi.cronRESTAPI.service.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zw.co.psmi.cronRESTAPI.entity.Task;
import zw.co.psmi.cronRESTAPI.dao.TaskDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import static zw.co.psmi.cronRESTAPI.scheduler.CronJob.COMMAND;
import zw.co.psmi.cronRESTAPI.service.TaskService;
//import zw.co.psmi.cronRESTAPI.scheduler.DynamicScheduler;

/**
 *
 * @author b1tfr34k
 */
@Service
public class TaskServiceImplementation implements TaskService, Trigger {

    private static final Logger logger = LoggerFactory.getLogger(TaskServiceImplementation.class);

    @Autowired
    private TaskDao taskDao;
    @Autowired
//    @Qualifier("readEntityManager")
    private EntityManager entityManager;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Creates or updates task in table tasks
    @Override
    public Task createTask(@RequestBody Task task) {
        if (task.active == null) {
            task.active = true;
        }
        return taskDao.save(task);
    }

    @Override
    public Task editTask(@RequestBody Task task) {
        return taskDao.save(task);
    }

    //Return task with matching ID from table tasks if it is active
    @Override
    public Task getById(@RequestParam("tId") long tId) {
        Task task = taskDao.findOne(tId);
        if (task.active == false) {
            return taskDao.findOne(0L);
        }

        return task;
    }

    //Returns all active tasks from table tasks
    @Override
    public List<Task> getAll() {
        Boolean active = true;
        List<Task> task = taskDao.findByActive(active);
        return task;
    }

    @Override
    public List<Task> remById(@RequestParam("tId") long tId) {
        Task task = taskDao.findOne(tId);
        task.active = false;
        taskDao.save(task);
        return getAll();
    }

    @Override
    public List<Task> delByReallyDeletingFromTheDataBaseService(@RequestParam("tId") long tId) {
        taskDao.deleteBytId(tId);
        return getAll();
    }

    //Not used in implementation
    @Override
    public Iterable<Task> getAllEvenInactive() {
        return taskDao.findAll();
    }

//    public void scheduleTask() {
//        Iterable<Task> task = taskDao.findAll();
//
//        for (Task t: task) {
//            String cmd = t.cmd;
//            String cronValue = t.taskTime;
//            logger.info("Task command (cmd) is:    {}", cmd);
//            logger.info("Task time (cronValue) is:    {}", cronValue);
//            
//            ds.configureTasks(cronValue, cmd);
//        }
//    }
//
    @Override
    public Date nextExecutionTime(TriggerContext tc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
    
//    public  List<Task> getTaskByQuery(JobExecutionContext context) throws JobExecutionException {
//        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
//        logger.info("In getTaskByQuery JobDataMap:    {}", dataMap);
//        String query = dataMap.getString(COMMAND);
//        logger.info("In getTaskByQuery query:    {}", query);
////        return taskDao.findQuery(query);
//        return jdbcTemplate.execute(query);
//    }
}
