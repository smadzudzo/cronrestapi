package zw.co.psmi.cronRESTAPI.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "tasks")
@Data
public class Task implements Serializable {

//    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long tId;

    @Column(name = "taskDesc")
    public String taskDesc;

    @Column(name = "taskTime")
    public String taskTime;

    @Column(name = "cmd")
    public String cmd;

    @Column(name = "taskType")
    public String taskType;
    
    @Column(name = "active")
    public Boolean active;

    protected Task() {
    }

//    public Task(String taskDesc, String taskTime, String cmd, String taskType) {
//        this.taskDesc = taskDesc;
//        this.taskTime = taskTime;
//        this.cmd = cmd;
//        this.taskType = taskType;
//        this.active = active;
//    }

    @Override
    public String toString() {
        return String.format("Task[tId=%d, taskDesc='%s', taskTime='%s', cmd='%s', taskType='%s', active='%b']", tId, taskDesc, taskTime, cmd, taskType, active);
    }
}
