/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.psmi.cronRESTAPI.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import zw.co.psmi.cronRESTAPI.entity.Task;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import zw.co.psmi.cronRESTAPI.scheduler.CronTriggerFromDB;
import zw.co.psmi.cronRESTAPI.service.TaskService;

/**
 *
 * @author b1tfr34k
 */

@RestController
public class TaskController {
    @Autowired
    CronTriggerFromDB ct;
    @Autowired
    private TaskService taskService;
    

    @RequestMapping(value = "/createTask")
    @ResponseBody
    @CrossOrigin("*")
    public Task newTask(@RequestBody Task task) {
        return taskService.createTask(task);
    }

    @RequestMapping(value = "/editTask")
    @ResponseBody
    @CrossOrigin("*")
    public Task editTasks(@RequestBody Task task) {
        return taskService.editTask(task);
    }

    @RequestMapping("/taskbyid")
    @ResponseBody
    @CrossOrigin("*")
    public Task findById(@RequestParam long tId) {
        return taskService.getById(tId);
    }

//    @RequestMapping("/taskall")
    @RequestMapping(value = {"/taskall", "/"})
    @ResponseBody
    @CrossOrigin("*")
    public List<Task> findAll() {
//        scheduleTask();
ct.CronTriggerSchedule();
//        new CronTriggerFromDB().CronTriggerSchedule();
        return taskService.getAll();
    }

    @RequestMapping("/delbyid")
    @ResponseBody
    @CrossOrigin("*")
    public List<Task> delById(@RequestParam("tId") long tId) {
        return taskService.remById(tId);
    }

    //Not used in implementation
    @RequestMapping("/taskAllEvenInactive")
    @ResponseBody
    @CrossOrigin("*")
    public Iterable<Task> findAllEvenInactive() {
        return taskService.getAllEvenInactive();
    }
    
    //Not used in implementation
    @RequestMapping("/delByReallyDeletingFromTheDataBase")
    @ResponseBody
    @CrossOrigin("*")
    public List<Task> delByReallyDeletingFromTheDataBase(@RequestParam("tId") long tId) {
        return taskService.delByReallyDeletingFromTheDataBaseService(tId);
    }
}
