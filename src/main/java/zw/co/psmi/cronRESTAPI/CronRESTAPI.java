package zw.co.psmi.cronRESTAPI;

import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CronRESTAPI {

    public static void main(String[] args) throws SchedulerException {
        SpringApplication.run(CronRESTAPI.class, args);
    }
}