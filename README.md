This is a cron project that schedules a job using a cron value stored in the database.

URLs accepted by controller are:
    1. /createTask
    2. /editTask
    3. /taskbyid?tId=2
    4. /taskall or /
    5. /delbyid?tId=2

Project uses default crontab format. It will throw an error if provided cron value does not conform to default format.